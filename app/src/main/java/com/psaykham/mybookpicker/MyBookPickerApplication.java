package com.psaykham.mybookpicker;

import com.psaykham.mybookpicker.components.ApplicationComponent;
import com.psaykham.mybookpicker.components.DaggerApplicationComponent;
import com.psaykham.mybookpicker.modules.ApplicationModule;

import android.app.Application;

/**
 * Books application.
 */
public class MyBookPickerApplication extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                                .applicationModule(new ApplicationModule(this))
                                .build();
    }


    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
