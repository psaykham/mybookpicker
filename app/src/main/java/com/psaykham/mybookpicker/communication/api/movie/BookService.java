package com.psaykham.mybookpicker.communication.api.movie;

import com.psaykham.mybookpicker.entities.Book;
import com.psaykham.mybookpicker.entities.Offers;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Service for interaction with the book API.
 */
public interface BookService {
    String ENDPOINT = "http://henri-potier.xebia.fr";
    String ISBNS = "isbns";

    @GET("/books")
    void getBooks(Callback<List<Book>> callback);

    @GET("/books/{isbns}/commercialOffers")
    void getDeals(@Path(ISBNS) String isbns,
                  Callback<Offers> callback);
}
