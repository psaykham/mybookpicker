package com.psaykham.mybookpicker.components;

import com.psaykham.mybookpicker.modules.ApplicationModule;
import com.psaykham.mybookpicker.ui.activities.CartActivity;
import com.psaykham.mybookpicker.ui.activities.CollectionActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(CartActivity cartActivity);
    void inject(CollectionActivity collectionActivity);
}
