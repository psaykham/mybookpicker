package com.psaykham.mybookpicker.constants;

/**
 * Contains constants related to prices.
 */
public class PriceConstants {

    public static final String PRICE_UNIT = "€";
    public static final String PRICE_FORMAT = "%.2f " + PRICE_UNIT;
}
