package com.psaykham.mybookpicker.entities;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Describes a book.
 */
public class Book implements Parcelable {

    @SerializedName("isbn")
    private String mIsbn;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("price")
    private float mPrice;

    @SerializedName("cover")
    private String mCover;

    protected Book(Parcel in) {
        mIsbn = in.readString();
        mTitle = in.readString();
        mPrice = in.readFloat();
        mCover = in.readString();
    }

    public String getIsbn() {
        return mIsbn;
    }

    public String getTitle() {
        return mTitle;
    }

    public float getPrice() {
        return mPrice;
    }

    public String getCover() {
        return mCover;
    }

    @Override
    public String toString() {
        return "Book{" +
                "mIsbn='" + mIsbn + '\'' +
                ", mTitle='" + mTitle + '\'' +
                ", mPrice=" + mPrice +
                ", mCover='" + mCover + '\'' +
                '}';
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mIsbn);
        dest.writeString(mTitle);
        dest.writeFloat(mPrice);
        dest.writeString(mCover);
    }
}
