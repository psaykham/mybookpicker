package com.psaykham.mybookpicker.entities;

import java.util.Map;

/**
 * Describes a cart.
 */
public interface Cart {

    /**
     * Adds book to the cart.
     * @param book
     */
    void addBook(Book book);

    /**
     * Increments the count of the book matching the given isbn.
     * @param isbn
     */
    void incrementBook(String isbn);

    /**
     * Completely removes the book from the cart.
     * @param isbn
     */
    void removeBook(String isbn);

    /**
     * Updates the count of the book matching the given isbn.
     * @param isbn
     * @param count
     */
    void updateBookCount(String isbn, int count);

    /**
     * @return the items inside the cart.
     */
    Map<String, CartItem> getCartItems();

    /**
     * @return true if the cart is empty, false otherwise.
     */
    boolean isEmpty();
}
