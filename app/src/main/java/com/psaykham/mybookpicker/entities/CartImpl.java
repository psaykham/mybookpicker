package com.psaykham.mybookpicker.entities;

import java.util.HashMap;
import java.util.Map;

/**
 * Describes a cart.
 */
public class CartImpl implements Cart {

    private Map<String, CartItem> mCartItems;

    public CartImpl() {
        mCartItems = new HashMap<>();
    }

    @Override
    public void addBook(Book book) {
        if (mCartItems.get(book.getIsbn()) != null) {
            // This means that the book was already added once, increment the count
            incrementBook(book.getIsbn());
        } else {
            // This book is added for the first time
            mCartItems.put(book.getIsbn(), new CartItem(book));
        }
    }

    @Override
    public void incrementBook(String isbn) {
        CartItem cartItem = mCartItems.get(isbn);
        cartItem.setCount(cartItem.getCount() + 1);
        mCartItems.put(isbn, cartItem);
    }

    @Override
    public void removeBook(String isbn) {
        if (mCartItems.containsKey(isbn)) {
            mCartItems.remove(isbn);
        }
    }

    @Override
    public void updateBookCount(String isbn, int count) {
        CartItem cartItem = mCartItems.get(isbn);
        cartItem.setCount(count);
        mCartItems.put(isbn, cartItem);
    }

    @Override
    public Map<String, CartItem> getCartItems() {
        return mCartItems;
    }

    @Override
    public boolean isEmpty() {
        return mCartItems.isEmpty();
    }
}
