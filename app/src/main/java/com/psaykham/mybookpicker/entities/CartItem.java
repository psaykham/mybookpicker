package com.psaykham.mybookpicker.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Describes a cart item.
 * A cart item contains:
 * - a book
 * - an amount of the book
 */
public class CartItem implements Parcelable {

    private Book mBook;
    private int mCount;

    public CartItem(Book book) {
        mBook = book;
        mCount = 1;
    }

    public Book getBook() {
        return mBook;
    }

    public void setBook(Book book) {
        mBook = book;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "mBook=" + mBook +
                ", mCount=" + mCount +
                '}';
    }

    public static final Creator<CartItem> CREATOR = new Creator<CartItem>() {
        @Override
        public CartItem createFromParcel(Parcel in) {
            return new CartItem(in);
        }

        @Override
        public CartItem[] newArray(int size) {
            return new CartItem[size];
        }
    };

    protected CartItem(Parcel in) {
        mBook = in.readParcelable(Book.class.getClassLoader());
        mCount = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mBook, 0);
        dest.writeInt(mCount);
    }
}
