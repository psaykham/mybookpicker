package com.psaykham.mybookpicker.entities;

import com.google.gson.annotations.SerializedName;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Describes an offer.
 */
public class Offer {

    public static final String PERCENTAGE = "percentage";
    public static final String MINUS = "minus";
    public static final String SLICE = "slice";

    @Retention(SOURCE)
    @StringDef({PERCENTAGE, MINUS, SLICE})
    public @interface OfferType {}

    @SerializedName("type")
    @OfferType
    private String mType;

    @SerializedName("sliceValue")
    private float mSliceValue;

    @SerializedName("value")
    private float mValue;

    private float mPriceWithOfferApplied;

    public @OfferType String getType() {
        return mType;
    }

    public float getSliceValue() {
        return mSliceValue;
    }

    public float getValue() {
        return mValue;
    }

    public float getPriceWithOfferApplied() {
        return mPriceWithOfferApplied;
    }

    public void setPriceWithOfferApplied(float priceWithOfferApplied) {
        mPriceWithOfferApplied = priceWithOfferApplied;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "mType='" + mType + '\'' +
                ", mSliceValue=" + mSliceValue +
                ", mValue=" + mValue +
                ", mPriceWithOfferApplied=" + mPriceWithOfferApplied +
                '}';
    }
}
