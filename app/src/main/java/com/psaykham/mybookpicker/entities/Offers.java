package com.psaykham.mybookpicker.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by psaykham on 15/02/16.
 */
public class Offers {

    @SerializedName("offers")
    private List<Offer> mOffers;

    public List<Offer> getOffers() {
        return mOffers;
    }

    @Override
    public String toString() {
        return "Offers{" +
                "mOffers=" + mOffers +
                '}';
    }
}
