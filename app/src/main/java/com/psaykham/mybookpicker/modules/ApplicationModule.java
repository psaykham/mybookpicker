package com.psaykham.mybookpicker.modules;

import com.psaykham.mybookpicker.entities.Cart;
import com.psaykham.mybookpicker.entities.CartImpl;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module initializing dependency injection.
 */
@Module
public class ApplicationModule {

    private Context mContext;

    public ApplicationModule(Context context) {
        mContext = context;
    }

    @Provides @Singleton Context provideApplicationContext() {
        return mContext;
    }

    @Provides @Singleton Cart provideCart() {
        return new CartImpl();
    }

}
