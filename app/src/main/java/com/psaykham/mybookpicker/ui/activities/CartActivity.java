package com.psaykham.mybookpicker.ui.activities;

import com.psaykham.mybookpicker.MyBookPickerApplication;
import com.psaykham.mybookpicker.R;
import com.psaykham.mybookpicker.communication.api.movie.BookService;
import com.psaykham.mybookpicker.entities.Cart;
import com.psaykham.mybookpicker.entities.CartItem;
import com.psaykham.mybookpicker.entities.Offer;
import com.psaykham.mybookpicker.entities.Offers;
import com.psaykham.mybookpicker.ui.compound_views.CartItemView;
import com.psaykham.mybookpicker.ui.listeners.OnCartItemUpdateListener;
import com.psaykham.mybookpicker.utils.CartUtils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Activity for the cart.
 */
public class CartActivity extends AppCompatActivity implements OnCartItemUpdateListener {

    private static final String LOG_TAG = CartActivity.class.getCanonicalName();

    @InjectView(R.id.cart_items) LinearLayout mLayoutCartItems;
    @InjectView(R.id.cart_total_with_offer) TextView mTextViewTotalWithOffer;
    @InjectView(R.id.cart_total) TextView mTextViewTotal;
    @InjectView(R.id.cart_empty) TextView mTextViewEmptyCart;

    @Inject protected Cart mCart;
    private BookService mBookService;
    private Offer mBestOffer;
    private float mTotalPrice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyBookPickerApplication) getApplication()).getApplicationComponent().inject(this);
        setContentView(R.layout.activity_cart);
        ButterKnife.inject(this);

        updateCart();
    }

    /**
     * Displays the cart.
     */
    private void updateCart() {
        clearCartItems();

        if (mCart.isEmpty()) {
            displayEmptyCartMessage();
            return;
        }

        updateCartItems(mCart);
        updatePrices();
    }

    /**
     * Displays the prices.
     */
    private void updatePrices() {
        mTotalPrice = CartUtils.getTotalPrice(mCart);
        displayTotal(mTotalPrice);

        // Load the commercial offers.
        loadOffers();
    }

    /**
     * Displays the cart for the given book list.
     * @param cart
     */
    private void updateCartItems(Cart cart) {
        // remove all children first
        clearCartItems();
        for (Map.Entry<String, CartItem> entry : cart.getCartItems().entrySet()) {
            CartItemView cartItemView = new CartItemView(this);
            cartItemView.setItem(entry.getValue());
            cartItemView.setOnCartItemUpdateListener(this);
            mLayoutCartItems.addView(cartItemView);
        }
    }

    /**
     * Clears the cart items from the view.
     */
    private void clearCartItems() {
        mLayoutCartItems.removeAllViews();
    }

    /**
     * Clears the cart item matching the given view.
     * @param view
     */
    private void clearCartItem(View view) {
        mLayoutCartItems.removeView(view);
        updatePrices();
    }

    /**
     * Loads the commercial offers for the current cart.
     */
    private void loadOffers() {
        // if the movie rest service is null, build it
        if (mBookService == null) {
            mBookService = new RestAdapter.Builder()
                    .setEndpoint(BookService.ENDPOINT)
                    .build()
                    .create(BookService.class);
        }

        mBookService.getDeals(CartUtils.getFormattedIsbnsForRequest(mCart), new Callback<Offers>() {
            @Override
            public void success(Offers offers, Response response) {
                Log.d(LOG_TAG, offers.toString());
                mBestOffer = CartUtils.getBestOffer(offers, mTotalPrice);
                displayTotalWithOfferApplied(mBestOffer);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(LOG_TAG, "Failed to load commercial offers");
            }
        });
    }

    /**
     * Displays the cart total for the given total price.
     * @param totalPrice
     */
    private void displayTotal(float totalPrice) {
        mTextViewTotal.setText(String.format(Locale.FRANCE, getString(R.string.cart_total), totalPrice));
    }

    /**
     * Displays the total with the given offer applied.
     * @param offer
     */
    private void displayTotalWithOfferApplied(Offer offer) {
        mTextViewTotalWithOffer.setText(String.format(Locale.FRANCE, getString(R.string.cart_total_with_offer_applied),
                offer.getType().toUpperCase(Locale.FRANCE),
                offer.getPriceWithOfferApplied()));
    }

    /**
     * Displays the "empty cart" message.
     */
    private void displayEmptyCartMessage() {
        mTextViewTotal.setVisibility(View.GONE);
        mTextViewTotalWithOffer.setVisibility(View.GONE);
        mTextViewEmptyCart.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUpdateCartItemCount(String isbn, int count) {
        mCart.updateBookCount(isbn, count);
        // No need to update the whole cart, just update the prices.
        updatePrices();
    }

    @Override
    public void onRemoveCartItem(CartItemView cartItemView, String isbn) {
        mCart.removeBook(isbn);
        clearCartItem(cartItemView);
        if (mCart.isEmpty()) {
            displayEmptyCartMessage();
        }
    }
}
