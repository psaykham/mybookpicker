package com.psaykham.mybookpicker.ui.activities;

import com.psaykham.mybookpicker.MyBookPickerApplication;
import com.psaykham.mybookpicker.R;
import com.psaykham.mybookpicker.communication.api.movie.BookService;
import com.psaykham.mybookpicker.entities.Book;
import com.psaykham.mybookpicker.entities.Cart;
import com.psaykham.mybookpicker.ui.adapters.BookAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Activity for the book collection.
 */
public class CollectionActivity extends AppCompatActivity {

    private static final String LOG_TAG = CollectionActivity.class.getCanonicalName();
    private static final String BUNDLE_KEY_BOOK_COLLECTION = "collection";

    @InjectView(R.id.books_gridview) RecyclerView booksGridView;

    @Inject protected Cart mCart;
    private BookService mBookService;
    private BookAdapter mBooksAdapter;
    private GridLayoutManager mLayoutManager;

    private List<Book> mBooks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyBookPickerApplication) this.getApplication()).getApplicationComponent().inject(this);
        setContentView(R.layout.fragment_collection);
        ButterKnife.inject(this);

        if (savedInstanceState != null) {
            mBooks = savedInstanceState.getParcelableArrayList(BUNDLE_KEY_BOOK_COLLECTION);
        }

        booksGridView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 3);
        booksGridView.setLayoutManager(mLayoutManager);

        if (mBooks == null) {
            loadBooks();
        } else {
            updateListing(mBooks);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_collection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_collection_cart:
                // Redirect to the cart activity only if the cart is not empty
                if (!mCart.isEmpty()) {
                    Intent intent = new Intent(this, CartActivity.class);
                    startActivity(intent);
                } else {
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.message_empty_cart), Snackbar.LENGTH_LONG).show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(BUNDLE_KEY_BOOK_COLLECTION, (ArrayList<? extends Parcelable>) mBooks);
    }

    /**
     * Loads books.
     */
    private void loadBooks() {
        if (mBookService == null) {
            mBookService = new RestAdapter.Builder()
                    .setEndpoint(BookService.ENDPOINT)
                    .build()
                    .create(BookService.class);
        }

        mBookService.getBooks(new Callback<List<Book>>() {
            @Override
            public void success(List<Book> books, Response response) {
                Log.d(LOG_TAG, books.toString());
                mBooks = books;

                // if page equals 1, this means we reload the entire listing
                // otherwise, we add it to the bottom of the list
                updateListing(mBooks);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(LOG_TAG, "Failed to load books");
            }
        });
    }

    /**
     * Updates the book listing.
     * @param books
     */
    private void updateListing(List<Book> books) {
        if (mBooksAdapter == null) {
            mBooksAdapter = new BookAdapter(this, books);
        }

        mBooksAdapter.setOnCartButtonListener(new BookAdapter.OnCartButtonListener() {
            @Override
            public void onClick(View v, int position) {
                Snackbar.make(findViewById(android.R.id.content), getString(R.string.message_added_to_cart), Snackbar.LENGTH_LONG).show();
                mCart.addBook(mBooks.get(position));
            }
        });

        booksGridView.setAdapter(mBooksAdapter);
    }
}
