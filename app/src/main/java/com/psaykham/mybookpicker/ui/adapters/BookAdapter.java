package com.psaykham.mybookpicker.ui.adapters;

import com.psaykham.mybookpicker.R;
import com.psaykham.mybookpicker.constants.PriceConstants;
import com.psaykham.mybookpicker.entities.Book;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Adapter for book collection.
 */
public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookHolder> {
    private Context mContext;
    private List<Book> mBooks;
    private OnCartButtonListener mOnCartButtonListener;

    public class BookHolder extends RecyclerView.ViewHolder {
        public ImageView cover;
        public TextView title;
        public TextView price;
        public ImageView addToCart;
        private OnCartButtonListener onCartButtonListener;

        public BookHolder(View view) {
            super(view);
            cover = (ImageView) view.findViewById(R.id.book_cover);
            title = (TextView) view.findViewById(R.id.book_title);
            price = (TextView) view.findViewById(R.id.book_price);
            addToCart = (ImageView) view.findViewById(R.id.add_to_cart);
            addToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCartButtonListener.onClick(v, getAdapterPosition());
                }
            });
        }

        public void setOnCartButtonListener(OnCartButtonListener onCartButtonListener) {
            this.onCartButtonListener = onCartButtonListener;
        }
    }

    public BookAdapter(Context context, List<Book> books) {
        mContext = context;
        mBooks = books;
    }

    @Override
    public BookHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_grid_book, parent, false);
        BookHolder holder = new BookHolder(view);
        holder.setOnCartButtonListener(mOnCartButtonListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final BookHolder holder, int position) {
        Book book = mBooks.get(position);

        if (!TextUtils.isEmpty(book.getCover())) {
            Picasso.with(mContext).load(book.getCover()).into(holder.cover);
        }

        holder.title.setText(book.getTitle());
        holder.price.setText(String.format(PriceConstants.PRICE_FORMAT, book.getPrice()));
    }

    @Override
    public int getItemCount() {
        return mBooks.size();
    }

    public void setOnCartButtonListener(OnCartButtonListener onCartButtonListener) {
        mOnCartButtonListener = onCartButtonListener;
    }

    public interface OnCartButtonListener {
        void onClick(View v, int position);
    }
}
