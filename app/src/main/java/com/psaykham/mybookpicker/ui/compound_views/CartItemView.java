package com.psaykham.mybookpicker.ui.compound_views;

import com.psaykham.mybookpicker.R;
import com.psaykham.mybookpicker.constants.PriceConstants;
import com.psaykham.mybookpicker.entities.Book;
import com.psaykham.mybookpicker.entities.CartItem;
import com.psaykham.mybookpicker.ui.listeners.OnCartItemUpdateListener;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Describes a cart item view.
 */
public class CartItemView extends LinearLayout {

    private CartItem mCartItem;
    private ImageView mCover;
    private TextView mTitle;
    private TextView mPrice;
    private Spinner mCount;
    private ImageView mItemRemoval;

    private OnCartItemUpdateListener mOnCartItemUpdateListener;

    private boolean mCountSpinnerInitialized = false;

    public CartItemView(Context context) {
        super(context);
        init();
    }

    public CartItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CartItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Sets the {@link OnCartItemUpdateListener}.
     * @param onCartItemUpdateListener
     */
    public void setOnCartItemUpdateListener(OnCartItemUpdateListener onCartItemUpdateListener) {
        mOnCartItemUpdateListener = onCartItemUpdateListener;
    }

    private void init() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.cell_cart_item, this, true);
        mCover = (ImageView) view.findViewById(R.id.book_cover);
        mTitle = (TextView) view.findViewById(R.id.book_title);
        mPrice = (TextView) view.findViewById(R.id.book_price);
        mCount = (Spinner) view.findViewById(R.id.book_count);
        mItemRemoval = (ImageView) view.findViewById(R.id.book_removal);
        setCount();
    }

    public void setItem(CartItem cartItem) {
        mCartItem = cartItem;
        final Book book = mCartItem.getBook();
        Picasso.with(getContext()).load(book.getCover()).into(mCover);
        mTitle.setText(book.getTitle());
        mPrice.setText(String.format(PriceConstants.PRICE_FORMAT, book.getPrice()));
        mCount.setSelection(mCartItem.getCount() - 1);
        mCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // USe this boolean, to avoid the update on the cart at the initialization of the view.
                if (mCountSpinnerInitialized) {
                    mOnCartItemUpdateListener.onUpdateCartItemCount(book.getIsbn(), position + 1);
                } else {
                    mCountSpinnerInitialized = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mItemRemoval.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnCartItemUpdateListener.onRemoveCartItem(CartItemView.this, book.getIsbn());
            }
        });
    }

    private void setCount() {
        Integer[] items = new Integer[10];
        for (int i = 0; i < items.length; i++) {
            items[i] = i + 1;
        }

        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, items);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCount.setAdapter(dataAdapter);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mOnCartItemUpdateListener = null;
    }
}
