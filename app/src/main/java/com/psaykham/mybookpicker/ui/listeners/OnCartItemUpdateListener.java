package com.psaykham.mybookpicker.ui.listeners;

import com.psaykham.mybookpicker.ui.compound_views.CartItemView;

/**
 * Contains callback methods to notify changes on a cart.
 */
public interface OnCartItemUpdateListener {

    /**
     * Called when the cart item is updated.
     * @param isbn
     * @param count
     */
    void onUpdateCartItemCount(String isbn, int count);

    /**
     * Called when the cart item has been removed.
     * @param cartItemView
     * @param isbn
     */
    void onRemoveCartItem(CartItemView cartItemView, String isbn);
}
