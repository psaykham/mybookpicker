package com.psaykham.mybookpicker.utils;

import com.psaykham.mybookpicker.entities.Book;
import com.psaykham.mybookpicker.entities.Cart;
import com.psaykham.mybookpicker.entities.CartItem;
import com.psaykham.mybookpicker.entities.Offer;
import com.psaykham.mybookpicker.entities.Offers;

import java.util.Map;

/**
 * Contains utility methods for cart.
 */
public class CartUtils {

    private static final String ISBN_SEPARATOR = ",";

    /**
     * @param cart
     * @return the total price for the given cart items list.
     */
    public static float getTotalPrice(Cart cart) {
        float totalPrice = 0;
        for (Map.Entry<String, CartItem> entry : cart.getCartItems().entrySet()) {
            CartItem cartItem = entry.getValue();
            Book book = cartItem.getBook();
            totalPrice += (book.getPrice() * cartItem.getCount());
        }

        return totalPrice;
    }

    /**
     * @param cart
     * @return the formatted ISBNs for the commercial offers request.
     */
    public static String getFormattedIsbnsForRequest(Cart cart) {
        String isbns = "";
        for (Map.Entry<String, CartItem> entry : cart.getCartItems().entrySet()) {
            CartItem cartItem = entry.getValue();
            Book book = cartItem.getBook();
            isbns += book.getIsbn() + ISBN_SEPARATOR;
        }

        return isbns;
    }

    /**
     * Calculates the best offer for the given offers list.
     * @param offers
     * @param initialPrice
     * @return the best offer for the given offers list.
     */
    public static Offer getBestOffer(Offers offers, float initialPrice) {
        Offer bestOffer = null;
        float minimalPrice = initialPrice;
        for (Offer offer : offers.getOffers()) {
            float calculatedPrice = initialPrice;
            switch (offer.getType()) {
                case Offer.PERCENTAGE:
                    calculatedPrice = calculatePriceWithPercentageApplied(offer, initialPrice);
                    break;

                case Offer.MINUS:
                    calculatedPrice = calculatePriceWithMinusApplied(offer, initialPrice);
                    break;

                case Offer.SLICE:
                    calculatedPrice = calculatePriceWithSliceApplied(offer, initialPrice);
                    break;
            }

            if (calculatedPrice < minimalPrice) {
                minimalPrice = calculatedPrice;
                bestOffer = offer;
                bestOffer.setPriceWithOfferApplied(calculatedPrice);
            }
        }

        return bestOffer;
    }

    /**
     * @param offer
     * @param initialPrice
     * @return the total price with the percentage offer applied.
     */
    public static float calculatePriceWithPercentageApplied(Offer offer, float initialPrice) {
        if (offer.getValue() > 0) {
            return initialPrice - ((offer.getValue() / 100) * initialPrice);
        }

        return initialPrice;
    }

    /**
     * @param offer
     * @param initialPrice
     * @return the total price with the minus offer applied.
     */
    public static float calculatePriceWithMinusApplied(Offer offer, float initialPrice) {
        if (offer.getValue() > 0) {
            return initialPrice - offer.getValue();
        }

        return initialPrice;
    }

    /**
     * @param offer
     * @param initialPrice
     * @return the total price with the slice offer applied.
     */
    public static float calculatePriceWithSliceApplied(Offer offer, float initialPrice) {
        if (offer.getValue() > 0) {
            int count = (int) (initialPrice / offer.getSliceValue());
            return initialPrice - (count * offer.getValue());
        }

        return initialPrice;
    }
}
